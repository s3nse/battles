#include "battle.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

#define battle_properties 4

typedef struct Battle {
    // struct tm date;
    char* battle_type;
    char* place;
    char* members;
    char* result;
} Battle;

Battle *create_battle(char *battle_type, char *place, char *members, char *result) {
    Battle *battle = (Battle *)malloc(sizeof(Battle));
    if (!battle) {
        return NULL;
    }
    battle->battle_type = battle_type;
    battle->place = place;
    battle->members = members;
    battle->result = result;
    return battle;
}

// Battle *create_battle_2(char **arr) {
//     Battle *battle = (Battle *)malloc(sizeof(Battle));
//     if (!battle) {
//         return NULL;
//     }
//     battle->battle_type = arr[0];
//     battle->place = arr[1];
//     battle->members = arr[2];
//     battle->result = arr[3];
//     return battle;
// }

static char input_char(FILE *stream) {
    char c = '\0';
    int result = 0;
    do {
        result = fscanf(stream, "%c", &c);
        if (result == EOF) {
            return EOF;
        }
    } while (result != 1);
    return c;
}

char **read_lines_from_file(const char *file_path, int lines_count) {
    FILE *file = fopen(file_path, "r");
    if (!file) {
        return NULL;
    }
    struct buffer {
        char *string;
        size_t size;
        size_t capacity;
    };
    char c = '\0';
    struct buffer buf_arr[lines_count];
    for (int i = 0; i < lines_count; i++) {
        buf_arr[i].string = NULL;
        buf_arr[i].size = 0;
        buf_arr[i].capacity = 0;
    }
    int j = 0;
    while (c = input_char(file), c != EOF) {
        if(c == '\n') {
            j++;
            continue;
        }
        
        if (buf_arr[j].size + 1 >= buf_arr[j].capacity) {
            size_t new_capacity = !buf_arr[j].capacity ? 1 : buf_arr[j].capacity * 2;
            char *tmp = (char *)malloc((new_capacity + 1) * sizeof(char));
            if (!tmp) {
                if (buf_arr[j].string) {
                    free(buf_arr[j].string);
                }
                fclose(file);
                return NULL;
            }
            if (buf_arr[j].string) {
                tmp = strcpy(tmp, buf_arr[j].string);
                free(buf_arr[j].string);
            }
            buf_arr[j].string = tmp;
            buf_arr[j].capacity = new_capacity;
        }
        buf_arr[j].string[buf_arr[j].size] = c;
        buf_arr[j].string[buf_arr[j].size + 1] = '\0';
        ++buf_arr[j].size;
    }
    fclose(file);
    char **result = (char **)malloc(sizeof(char *) * lines_count);
    if (!result) {
        return NULL;
    }
    for (int i = 0; i < lines_count; i++) {
        result[i] = buf_arr[i].string;
    }
    return result;
}

// Battle *create_battle_from_file(const char *file_path) {
//     char** str_arr = read_lines_from_file(file_path, battle_properties);
//     Battle *battle = create_battle_2(str_arr);
//     // for (int i = 0; i < battle_properties; i++) {
//     //     free(str_arr[i]);
//     // }
//     // free(str_arr);
//     return battle;
// }

void print_battle_info(Battle *battle) {
    printf("\n");
    printf("Вид сражения:                   %s\n", battle->battle_type);
    printf("Место генерального сражения:    %s\n", battle->place);
    printf("Страны участники:               %s\n", battle->members);
    printf("Результат:                      %s\n", battle->result);
}

void search_battles(const char *search_str, Battle *battles[], size_t arr_size) {
    for (size_t i = 0; i < arr_size; i++) {
        if (strstr(battles[i]->place, search_str) ||
        strstr(battles[i]->members, search_str) ||
        strstr(battles[i]->battle_type, search_str)) {
            print_battle_info(battles[i]);
        }
    }
}
