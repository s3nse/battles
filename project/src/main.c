#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "battle.h"
#include <string.h>

int main() {
    Battle* battle1 = create_battle("сухопутное", "Бородино1", "Россия, Франция", "Победила Россия");
    Battle* battle2 = create_battle("сухопутное", "Бородино2", "Россия, Франция", "Победила Россия");
    Battle* battle3 = create_battle("сухопутное", "Битва за", "Россия, Франция", "Победила Россия");
    char** str = read_lines_from_file("../text.txt", 4);
    Battle* battle4 = create_battle(str[0], str[1], str[2], str[3]);
    Battle* battles[] = { battle1, battle2, battle3, battle4 };
    size_t arr_size = sizeof(battles) / sizeof(Battle *);
    search_battles("Бороди", battles, arr_size);
    free(battle1);
    free(battle2);
    free(battle3);
    free(battle4);
    for (int i = 0; i < 4; ++i) {
        free(str[i]);
    }
    free(str);
    return 0;
}
