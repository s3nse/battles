#ifndef BATTLE_H
#define BATTLE_H

#include <stdio.h>

typedef struct Battle Battle;

Battle *create_battle(char *, char *, char *, char *);
char **read_lines_from_file(const char *, int);
// Battle *create_battle_2(char **);
// Battle *create_battle_from_file(const char *);
void print_battle_info(Battle *);
void search_battles(const char *, Battle **, size_t);

#endif
